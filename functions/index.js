



const  distance_matrix =require('google-distance-matrix');
const functions=require('firebase-functions')
const admin=require('firebase-admin');
admin.initializeApp();
distance_matrix.key('AIzaSyBoamspBjGAp6mltFLFDx-aNA8cLetyvLk')

// // // Create and Deploy Your First Cloud Functions
// // // https://firebase.google.com/docs/functions/write-firebase-functions
// //
// // exports.helloWorld = functions.https.onRequest((request, response) => {
// //  response.send("Hello from Firebase!");
// // });

let mainCounter=0
let wasteStack=[]
let wasteNodeList=[]


let nextNode
let pVisitedNodes=[]
let tVisitedNodes=[]
let tVisitedNodesII=[]//second phase
let recommendedWastes=[]//all the wastes from waste node of firestore



// var Waste = /** @class */ (function () {
 
//     function Waste( SourceLat, SourceLon, SourceId, SourceStatus,SourceType,Distance,Duration,Paths) {
//         this.sourceId = SourceId;
//         this.sourceLat = SourceLat;
//         this.sourceLon = SourceLon;
//         this.sourceStatus = SourceStatus;
//         this.distance=Distance;
//         this.duration=Duration;
//         this.sourceType=SourceType;
//         this.paths=Paths//it holds the type Waste
        
//     }
//     return Waste;
// }());

// var WasteNode = /** @class */ (function () {
//     //this is used by nearby wastes only
//     function WasteNode(WasteList,SourceId) {
//         this.wasteList=WasteList
//         this.sourceId=SourceId
//     }
//     return WasteNode;
// }());

// var Truck = /** @class */ (function () {

//     function Truck(TruckPosLat,TruckPosLon,TruckId,TruckDriverName,TruckDriverPnumber,Truckwastes,Status,Distance,Duration) {
//         this.truckPosLat=TruckPosLat
//         this.truckPosLon=TruckPosLon
//         this.truckId=TruckId;
//         this.truckDriverName=TruckDriverName;
//         this.truckDriverPnumber=TruckDriverPnumber;
//         this.truckwastes=Truckwastes;
//         this.status=Status;
//         this.distance=Distance;
//         this.duration=Duration;


//     }
//     return Truck;
// }());


exports.newWasteNode=functions.firestore.document('wastes/{newdoc}').onCreate((newWasteSnap,context)=>{
    //change testwaste to wastes

   return admin.firestore().collection('wastes').get().then(snapshot=>{
        let origins=[]
        let destinations=[]
        let wasteList=[]//holds waste object
        let wasteForUpdate=[]
        let recommendedWastes=[]
        let newWaste=newWasteSnap.data()
        /*recommendedWastes is same as wasteList*/

        /*what if the sent location is not valid */


    
        //for first time check only
        snapshot.forEach(doc=>{
            console.log(doc.data().sourceLat+','+doc.data().sourceLon+"\n")
            wasteList.push(doc.data())
            destinations.push(doc.data().sourceLat+','+doc.data().sourceLon)
        })
        origins.push(newWaste.sourceLat+','+newWaste.sourceLon)//replace with new node 
        
        //if the call is for the first time
        if(wasteList.length==1){

            return new Promise((resolve,reject)=>{
            
                distance_matrix.matrix(origins,destinations,function(err,response){
                    console.log("json response "+JSON.stringify(response))
                    if(response['status']=="OK"){
                        wasteList[0].address=response['origin_addresses'][0]//store value not array
                   }

                   resolve('distance matrix finished for first single waste in database')
                   
                })
            }).then((status)=>{
                console.log(status)
                //update the new waste node
            return  admin.firestore().collection('wastes').doc(newWasteSnap.id).update({address:wasteList[0].address}).then((updateSnap)=>{
                console.log('Program ended for single waste ')
                
            })

        })
    }
    else{
            //if there are more than one wastes

        return new Promise((resolve,reject)=>{

            //clear the wastelist and destinations array
            wasteList=[]
            destinations=[]
            let counter=0
            snapshot.forEach(eachDoc=>{
                if(!(newWaste.sourceId===eachDoc.data().sourceId)){
                    wasteList.push(eachDoc.data())
                    destinations.push(eachDoc.data().sourceLat+','+eachDoc.data().sourceLon)
                }
                
            })
            
            
            distance_matrix.matrix(origins,destinations,function(err,response){
                console.log("json response "+JSON.stringify(response))
                
                if(response['status']=="OK"){
                    //update the newnode address
                    newWaste.address=response['origin_addresses'][0]                    
                    var rowsObject=response['rows']
                    rowsObject.forEach(eachrows=>{
                        var elements=eachrows['elements']
                        elements.forEach((eachElements,index)=>{// value and counter as arg
                        if(eachElements.status=="OK"){
                        console.log('paths during distance matrix '+wasteList[index].paths)
                        wasteList[index].distance=eachElements.distance.value//dont update on wastelist this results in null in other variables
                        wasteList[index].duration=eachElements.duration.value//by default other variable in tempholder are null like paths =null
                        recommendedWastes.push(wasteList[index])//hold the result for that object temporary
                        }else{
                            console.log('cannot map the waste with newnode')
                            /*In future also check the code if the distance is null or not in below codes*/
                            wasteList[index].distance=null
                            wasteList[index].duration=null
                            recommendedWastes.push(wasteList[index])//hold the result for that object temporary                            
                        }
                           
                       })
                    })
                    resolve('distance matrix finished')
               }else{
                   console.log("error in the distance matrix calls")
                   reject('error')
               }
            })
        }).then((status)=>{


            console.log('updating the new waste')
            recommendedWastes.sort((waste1,waste2)=>{
                return waste1.distance-waste2.distance
            })

            //recommended contains each.paths whose inner json array needs to be validated

            let newWasteHolder=[]
            recommendedWastes.forEach((each,index)=>{
                newWasteHolder.push(
                    {
                        sourceId:recommendedWastes[index].sourceId,
                        sourceLat:recommendedWastes[index].sourceLat,
                        sourceLon:recommendedWastes[index].sourceLon,
                        sourceStatus:recommendedWastes[index].sourceStatus,
                        sourceWeight:recommendedWastes[index].sourceWeight,
                        sourceType:recommendedWastes[index].sourceType,
                        address:recommendedWastes[index].address,
                        distance:recommendedWastes[index].distance,
                        duration:recommendedWastes[index].duration,
                        paths:null
                    }
                )
            })
            //this avoid objects paths as null without affecting the wastelist or recommended waste
            console.log('updating mutual wastes')
            
            newWaste.paths=JSON.stringify(newWasteHolder)
            wasteList.forEach((mutualWaste,index)=>{
                
                if(mutualWaste.paths==null){
                    //add new waste to this wastlist[index]
                    console.log('if called')
                    wasteList[index].paths=JSON.stringify([
                        {
                            sourceId:newWaste.sourceId,
                            sourceLat:newWaste.sourceLat,
                            sourceLon:newWaste.sourceLon,
                            sourceStatus:newWaste.sourceStatus,
                            sourceWeight:newWaste.sourceWeight,
                            sourceType:newWaste.sourceType,
                            distance:mutualWaste.distance,
                            duration:mutualWaste.duration,
                            address:newWaste.address,
                            paths:null

                        }

                    ])//conver into jsonarray string
                }else{
                    //grab the waste[index].paths validate,add,sort and update
                    //validation
                    console.log('else called')
                    mutualWaste.paths=mutualWaste.paths
                        .replace(/\\n/g, "\\n")
                        .replace(/\\'/g, "\\'")
                        .replace(/\\"/g, '\\"')
                        .replace(/\\&/g, "\\&")
                        .replace(/\\r/g, "\\r")
                        .replace(/\\t/g, "\\t")
                        .replace(/\\b/g, "\\b")
                        .replace(/\\f/g, "\\f")
                        container=JSON.parse(mutualWaste.paths)

                        console.log('old paths: '+JSON.stringify(container))
                

                    //push
                    container.push(
                        {   
                            sourceId:newWaste.sourceId,
                            sourceLat:newWaste.sourceLat,
                            sourceLon:newWaste.sourceLon,
                            sourceStatus:newWaste.sourceStatus,
                            sourceWeight:newWaste.sourceWeight,
                            sourceType:newWaste.sourceType,
                            address:newWaste.address,
                            distance:mutualWaste.distance,
                            duration:mutualWaste.duration,
                            paths:null
                        }
                    )
                    //changing the contents of container will reflect in wastelist as well
                    //because mutualwaste is an object
                    //sort
                    container.sort((waste1,waste2)=>{
                        return waste1.distance-waste2.distance
                    })

                    //update 
                    wasteList[index].paths=JSON.stringify(container)
                    console.log('new paths: '+wasteList[index].paths)
                                                            
                }
            })
     
   
    let DOC_ID_CONTAINER=[]
    snapshot.forEach((each,index)=>{
        if(each.data().sourceId!=newWaste.sourceId){
            DOC_ID_CONTAINER.push(each.id)            
        }
    })

    let counter=0
    return admin.firestore().doc('wastes/'+newWasteSnap.id).
    update({paths:newWaste.paths,address:newWaste.address}).then((updateSnap)=>{

        DOC_ID_CONTAINER.forEach((DOC_ID,index)=>{
            return admin.firestore().doc('wastes/'+DOC_ID).
            update({paths:wasteList[index].paths}).then((updateMutualsnap)=>{
                ++counter
                if(counter==wasteList.length){console.log('Program successfully ended')}
            })
        })

    })


})

}//else ends

}).catch(()=>{
    console.log('error in the code')

    })

})

exports.newPickerRequest=functions.firestore.document('testPickers/{newPickerRequest}').onUpdate((updateSnapshot,context)=>{

    let truckDriverNodeId=updateSnapshot.after.id
    let wasteList=[]//stores all the wastes
    let recommendedWastes=[]//same as wasteList
    let destinations=[]
    let origin=[]

    //avoid infinte looping
    if(!updateSnapshot.after.data().selfRequest || updateSnapshot.after.data()==null){return}
    console.log('Request came from '+updateSnapshot.after.data().truckDriverName)
    
        origin.push(updateSnapshot.after.data().truckPosLat+','+updateSnapshot.after.data().truckPosLon)
        console.log('origin location: '+origin)
        console.log('Request came from id: '+updateSnapshot.after.id)
        
        return admin.firestore().collection('wastes').get().then((wasteSnapshot,context)=>{
      
    
            wasteSnapshot.forEach(eachWaste=>{
                wasteList.push(eachWaste.data())
                destinations.push(eachWaste.data().sourceLat+','+eachWaste.data().sourceLon)
            })
    
            //this then is only called when the above code has resolve called so no need for new promise to wrap distance matrix

            return new Promise((resolve,reject)=>{
            distance_matrix.matrix(origin,destinations,function(err,response){
                console.log("json response "+JSON.stringify(response))
                if(response['status']=="OK"){
                    var rowsObject=response['rows']
                    rowsObject.forEach(eachrows=>{
                        var elements=eachrows['elements']
                        elements.forEach((eachElements,index)=>{// value and counter as arg
                        if(eachElements.status=="OK"){
                            wasteList[index].distance=eachElements.distance.value
                            wasteList[index].duration=eachElements.duration.value
                            recommendedWastes.push(wasteList[index])//hold the result for that object temporary
                        }else{
                            /*In future also check the code if the distance is null or not in below codes*/
                            wasteList[index].distance=null
                            wasteList[index].duration=null
                            recommendedWastes.push(wasteList[index])//hold the result for that object temporary
                        }
                           
                       })
                    })
                    resolve('distance matrix finished')
               }else{
                   console.log("error in the distance matrix calls")
                   reject('error')
               }
            })


        }).then((status)=>{
            console.log(status)
    
            /*just update the truckdriverNode after sorting*/
            //note : event if the json response is invalid i.e cannot map then also it works but gets all the waste as it is no sorting works
            //because distance :null
            recommendedWastes.sort((waste1,waste2)=>{
                return waste1.distance-waste2.distance
            })

            //fix the paths json string which has invalid escape characters
            recommendedWastes.forEach((eachWaste,index)=>{
                recommendedWastes[index].paths=JSON.parse(eachWaste.paths
                .replace(/\\n/g, "\\n")
                .replace(/\\'/g, "\\'")
                .replace(/\\"/g, '\\"')
                .replace(/\\&/g, "\\&")
                .replace(/\\r/g, "\\r")
                .replace(/\\t/g, "\\t")
                .replace(/\\b/g, "\\b")
                .replace(/\\f/g, "\\f")
                )
                
            })
            

            let recommendedWastesValidJson=JSON.stringify(recommendedWastes)//into json string
            
            console.log('Final return json string '+recommendedWastesValidJson)
            
            return admin.firestore().doc('testPickers/'+truckDriverNodeId).update({truckwaste:recommendedWastesValidJson,selfRequest:false}).then((setSnap)=>{
                console.log('overrall program finished')
            })
        })

    })
    


})



        